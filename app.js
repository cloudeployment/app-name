const express = require('express')
const app = express()
const port = 3000

app.get('/', (req, res) => res.send('Finalmente chegamos ao fim do Continuous Deployment!'))

app.listen(port, () => console.log(`Example app listening on port ${port}!`))
